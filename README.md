# vuetodo

## prerequisites

1. database
it needs derby database 'derby' running at port 1527 (standard)


## install

1. setup js dependencies

```
cd js
npm install
```

## JS development

1. JS project might be opened in IDE (webstorm) from folder js

2. go into folder js
    ```
    cd js
    ``` 
3. start UI server
<br>*(might be done from IDE to get hot deployment)*
    ```
    npm run serve
    ```
4. start proxy and server
- with mock server
> ```
> cd server
> ./start-mock-application.sh
>```
> starts
> - ui on port 8080
> - proxy on port 3000
> 
    
or 
- with real server
>    - start proxy
>    
>    ```
>    cd server    
>    ./start-proxy-for-real.sh
>    ```
> - start rest server **com.test.Application** class in IDE

> starts
> - ui on port 8080
> - proxy on port 3000
> - rest server on port 9000
> 

#### open in browser
http://localhost:3000

## build JS bundle from js sources

```
./build-js.sh
```
builds JS part into static resources
 
### start built application

1. start **com.test.Application** class in IDE
2. open http://localhost:9000
 