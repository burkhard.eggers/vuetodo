import Details from "./components/Details/index";
import App from './components/App';
import Login from './components/Login';
import VueRouter from "vue-router";

/* eslint-disable no-debugger */

const routes = [
  {
    path: '**/todo/:id',
    components: {
      dialog: Details,
      view: App
    }
  },
  {
    path: '**/login',
    components: {
      view: Login
    }
  },
  {
    path: '**',
    components: {
      view: App
    }
  }
];


const router = new VueRouter({
  routes,
  //mode: 'hash'
  mode: 'history'
});

const afterHooks = [];

export const addAfterEach = (after) => {
  afterHooks.push(after);
};

export const removeAfterEach = (after) => {
  const ind = afterHooks.indexOf(after);
  afterHooks.splice(ind, 1);
};

const afterEach = (to, from) => {
    afterHooks.forEach(hook => hook(to, from));
};

router.afterEach(afterEach);

export default router;
