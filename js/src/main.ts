import Vue from 'vue'
import Vuex from 'vuex';
import VueRouter from 'vue-router';
// @ts-ignore
import Border from './components/Border.vue';
import getStore from './vuex/store';
import i18n from './i18n';
import 'vue-material/dist/vue-material.min.css'
import 'vue-material-design-icons/styles.css';
import Logger from 'js-logger';
import moment from 'moment';
import './vue-material-fix.css';
import router from './router';

Vue.use(Vuex);
Vue.use(VueRouter);

const store = getStore();

Logger.useDefaults({
  defaultLevel: Logger.INFO,
  formatter: (messages: any[], { name}) => {
    const date = moment().format('h:mm:ss'); /* full format 'MMMM Do YYYY, h:mm:ss'*/
    const effName = name || '';
    const rest = '                                  '.substr(0, 15 - effName.length);
    messages.unshift(`[${name}] ${rest} ${date} \t`)
  }
});

new Vue({
  el: '#app',
  store,
  i18n,
  router,
  components: { Border },
  template: '<Border />'
});
