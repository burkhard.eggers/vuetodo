export default {
  todoSing: 'To-Do',
  todoPlural: 'To-Dos',
  subtasksing: 'Subtarea',
  subTaskPlural: 'Subtareas',
  description: 'descripción',
  summary: 'Nombre',
  modals: {
    ok: 'Ok',
    cancel: 'Cancelar'
  },
  editor:{
    requiredField: 'obligatorio',
    correctFields: 'Corrija los siguientes campos'
  },
  main: {
    noTodos: 'no hay @:todoPlural',
    chooseLanguage: 'selecccione idioma',
    header: '@:todoPlural',
    addTodo: 'Nombre del nuevo @:todoSing',
    addTodoTitle: 'Nuevo @:todoSing',
    addTodoOkText: 'seguir',
    descriptionColumnHeader: '@:description',
    summaryColumnHeader: '@:summary',
    subTaskColumnHeader: '@:subTaskPlural',
    addTooltip: '@:main.addTodoTitle',
    addSubTaskPrompt: 'añadir @:subtasksing a {summary}',
    addSubTaskPromptOkText: 'Añadir',
    addSubTaskMenuOption: 'Añadir @:subtasksing',
    openDetailsTooltip: 'Detalles de {summary}',
    menuTooltip: 'Optciónes de {summary}',
    deleteMenuItem: 'Borrar',
    editMenuItem: 'Editar',
    deleteTodoPrompt: 'Borrar @:todoSing {summary}?',
    deleteTodoPromptOkText: 'Borrar',
    requestInProgress: 'pedido de datos...',
    errorHappened: 'pasó un error',
    errorDetailsTitle: 'Error',
    errorShowDetails: 'ver detalles',
    errorReload: 'cargar de nuevo',
    errorClose: 'cerrar',
    errorMsg: 'Mensaje',
    errorType: 'Tipo de Exception',
    logout: 'salir',
    adjustments: 'Adjustes',
    language: 'idioma escogido'
  },
  details: {
    header: '@:todoSing {summary}',
    deleteTodoTooltip: 'borrar {summary}',
    deleteTodoPrompt: 'Borrar {summary}?',
    summaryPlaceholder: 'editar @:summary',
    descriptionPlaceholder: 'agregar @:description',
    save: 'Archivar',
    close: 'Cerrar',
    deleteTodoPromptDeleteButton: 'Borrar'
  },
  subtasks: {
    readyColumHeader: 'Listo',
    nameColumHeader: 'Nombre',
    header: '@:subTaskPlural',
    addTooltip: 'Agregar @:subtasksing',
    newSubtaskPrompt: 'Agregar @:subtasksing',
    newSubtaskAleadyThere: 'Ya hay @:subtasksing con el nombre {name} en el @:todoSing. Por favor use otro nombre'
  },
  login: {
    title: 'Login',
    user: 'Nombre',
    password: 'Contraseña',
    userPlaceholder: 'nombre del usuario',
    passwordPlaceholder: 'entra su contraseña',
    button: 'Enviar',
    badCredentials: 'nombre/contraseña falso'
  }
};
