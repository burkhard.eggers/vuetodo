export default {
  todoSing: 'To-Do',
  todoPlural: 'To-Dos',
  subtasksing: 'sub task',
  subTaskPlural: 'sub tasks',
  description: 'description',
  summary: 'summary',
  modals: {
    ok: 'Ok',
    cancel: 'Cancel'
  },
  editor:{
    requiredField: 'Required field',
    correctFields: 'Please correct the following fields'
  },
  main: {
    noTodos: 'No @:todoPlural',
    chooseLanguage: 'Choose language',
    header: '@:todoPlural',
    addTodo: 'Summary of new @:todoSing',
    addTodoTitle: 'New @:todoSing',
    addTodoOkText: 'Add',
    descriptionColumnHeader: '@:description',
    summaryColumnHeader: '@:summary',
    subTaskColumnHeader: '@:subTaskPlural',
    addTooltip: '@:main.addTodoTitle',
    addSubTaskPrompt: 'Add @:subtasksing to {summary}',
    addSubTaskPromptOkText: 'Add',
    addSubTaskMenuOption: 'Add @:subtasksing',
    openDetailsTooltip: 'Details of {summary}',
    menuTooltip: 'Actions for {summary}',
    deleteMenuItem: 'Delete',
    editMenuItem: 'Edit',
    deleteTodoPrompt: 'Delete @:todoSing {summary}?',
    deleteTodoPromptOkText: 'Delete',
    requestInProgress: 'request in progress',
    errorHappened: 'An error has occured',
    errorShowDetails: 'show details',
    errorDetailsTitle: 'Error',
    errorReload: 'reload page',
    errorClose: 'close',
    errorMsg: 'Message',
    errorType: 'Exception Type',
    logout: 'logout',
    adjustments: 'Adjustments',
    language: 'chosen language'
  },
  details: {
    header: '@:todoSing {summary}',
    deleteTodoTooltip: 'Delete {summary}',
    deleteTodoPrompt: 'Delete {summary}?',
    summaryPlaceholder: 'enter @:summary',
    descriptionPlaceholder: 'add @:description',
    save: 'Save',
    close: 'Close',
    deleteTodoPromptDeleteButton: 'Delete'
  },
  subtasks: {
    readyColumHeader: 'Ready',
    nameColumHeader: 'Name',
    header: '@:subTaskPlural',
    addTooltip: 'Add @:subtasksing',
    newSubtaskPrompt: 'Add @:subtasksing',
    newSubtaskAleadyThere: 'A @:subtasksing with name {name} is already part of @:todoSing. Please choose another name'
  },
  login: {
    title: 'Login',
    user: 'Username',
    password: 'Password',
    userPlaceholder: 'enter username',
    passwordPlaceholder: 'enter password',
    button: 'Submit',
    badCredentials: 'You entered bad credentials'
  }
};
