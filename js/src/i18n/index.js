import Vue from 'vue'
import VueI18n from 'vue-i18n'
import de from './de';
import en from './en';
import es from './es';
import jsLog from 'js-logger';
import Cookies from 'cookie-universal';

Vue.use(VueI18n);

const cookies = new Cookies();

const messages = {
  en, de, es
};

export const COOKIE_LANGUAGE_NAME = 'todo-language';

const language = () => {
  const log = jsLog.get('i18n');
  const navLanguage = cookies.get(COOKIE_LANGUAGE_NAME) || window.navigator.userLanguage || window.navigator.language;
  const languages = Object.keys(messages);
  log.info('languages', languages);
  let language = languages[0];
  let matches = 0;
  languages.forEach(lang => {
    let match = 0;
    if(lang === navLanguage) {
      match = 10;
    } else if(navLanguage.startsWith(lang)) {
      match = 5;
    }
    if(match >= matches) {
      matches = match;
      log.info('lang', lang, match);
      language = lang;
    }
  });
  return language;
};

// Create VueI18n instance with options
export default new VueI18n({
  locale: language(),
  messages,
});

