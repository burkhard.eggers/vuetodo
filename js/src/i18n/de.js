export default {
  todoSing: 'To-Do',
  todoPlural: 'To-Dos',
  subtasksing: 'Unteraufgabe',
  subTaskPlural: 'Unteraufgaben',
  description: 'Beschreibung',
  summary: 'Name',
  modals: {
    ok: 'Ok',
    cancel: 'Abbrechen'
  },
  editor:{
    requiredField: 'Das Feld muss gesetzt sein',
    correctFields: 'Bitte korrigieren Sie die folgenden Felder'
  },
  main: {
    noTodos: 'keine @:todoPlural',
    chooseLanguage: 'Sprache auswählen',
    header: '@:todoPlural',
    addTodo: 'Namen von neuem @:todoSing',
    addTodoTitle: 'Neues @:todoSing',
    addTodoOkText: 'Weiter',
    descriptionColumnHeader: '@:description',
    summaryColumnHeader: '@:summary',
    subTaskColumnHeader: '@:subTaskPlural',
    addTooltip: '@:main.addTodoTitle',
    addSubTaskPrompt: '@:subtasksing zu {summary} hinzufügen',
    addSubTaskPromptOkText: 'Hinzufügen',
    addSubTaskMenuOption: '@:subtasksing hinzufügen',
    openDetailsTooltip: 'Details von {summary}',
    menuTooltip: 'Optionen für {summary}',
    deleteMenuItem: 'Löschen',
    editMenuItem: 'Editieren',
    deleteTodoPrompt: '@:todoSing {summary} löschen?',
    deleteTodoPromptOkText: 'Löschen',
    requestInProgress: 'Anfrage läuft',
    errorHappened: 'Ein Fehler ist aufgetreten',
    errorDetailsTitle: 'Fehler',
    errorShowDetails: 'Details anzeigen',
    errorReload: 'Seite neu laden',
    errorClose: 'Schließen',
    errorMsg: 'Text',
    errorType: 'Ausnahmetyp',
    logout: 'Abmelden',
    adjustments: 'Einstellungen',
    language: 'gewählte Sprache'
  },
  details: {
    header: '@:todoSing {summary}',
    deleteTodoTooltip: '{summary} löschen',
    deleteTodoPrompt: '{summary} löschen?',
    summaryPlaceholder: '@:summary eingeben',
    descriptionPlaceholder: '@:description hinzufügen',
    save: 'Speichern',
    close: 'Schließen',
    deleteTodoPromptDeleteButton: 'Löschen',
  },
  subtasks: {
    readyColumHeader: 'Erledigt',
    nameColumHeader: 'Name',
    header: '@:subTaskPlural',
    addTooltip: '@:subtasksing hinzufügen',
    newSubtaskPrompt: '@:subtasksing hinzufügen',
    newSubtaskAleadyThere: '@:subtasksing {name} ist bereits in diesem @:todoSing enthalten.' +
      'Bitte verwenden Sie einen andere Namen'
  },
  login: {
    title: 'Login',
    user: 'Name',
    password: 'Password',
    userPlaceholder: 'Namen eingeben',
    passwordPlaceholder: 'Password eingeben',
    button: 'Abschicken',
    badCredentials: 'Name/Password falsch'
  }
};
