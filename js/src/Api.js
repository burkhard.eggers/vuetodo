import jsLog from 'js-logger';
import axios from 'axios';
import getStore from './vuex/store';
import router from './router';
import { TIME_TO_SHOW_SPINNER_FOR_LONG_TAKING_REQUESTS, TIME_TO_HIDE_ERROR_SNACKBAR_AUTOMATICALLY } from './Constants';

const log = jsLog.get('Api');

//eslint-disable-next-line
const CONTEXT = false && process.env.NODE_ENV === 'production' ? '' : 'rest';

const request = async ({ path, root, method = 'GET', data }) => {
  const url = root ? path : `/${CONTEXT}${path}`;
  log.info(`request ${method} ${url}`);
  const store = getStore();
  const timer = setTimeout(() => {
    store.dispatch('startRequest');
  }, TIME_TO_SHOW_SPINNER_FOR_LONG_TAKING_REQUESTS);
  // eslint-disable-next-line no-async-promise-executor
  return new Promise((resolve, reject) => {
    log.info('start request ' + url + ' (' + method + ')');
    if (data) log.info('data', data);
    axios[method.toLowerCase()](url, data).then(resp => {
      log.info('request returned ', resp);
      store.dispatch('endRequest');
      resolve(resp);
      clearTimeout(timer);
    }).catch(async ({ response }) => {
      const { data, status, config } = response;
      log.error(`status:\t${status}`);
      clearTimeout(timer);
      switch (status) {
        case 409: {
          // exception caught by exception handler on server
          log.error(`message:\t${data.errorMsg}`);
          store.dispatch('error', data);
          break;
        }
        case 401: {
          debugger;
          log.info('unauthorized -> /login');
          store.dispatch('endRequest');
          router.push('/login');
          break;
        }
        default: {
          log.error('\tresponse', JSON.stringify(config));
        }
      }
      setTimeout(() => {
        if (store.state.request.error.showSnack) {
          log.info('close error details from API');
          store.dispatch('closeErrorDetails');
        }
      }, TIME_TO_HIDE_ERROR_SNACKBAR_AUTOMATICALLY);
      reject();
      // resolve({ data: {} });
    });
  });
};

export const getTodos = () => request({ path: '' });

export const updateTodo = (todo) => request({ path: '', method: 'PUT', data: todo });

export const deleteTodo = (todoId) => request({ path: `/${todoId}`, method: 'DELETE' });

export const addSubtask = (todoId, name) => request({ path: `/${todoId}/subtask`, method: 'PUT', data: { name } });

export const newTodo = (name) => request({ path: '', method: 'POST', data: { summary: name } });

export const login = (user, passwordHash) => request({ path: '/auth', root: true, method: 'POST', data: { user, passwordHash } });

export const logout = () => request({ path: '/auth', root: true, method: 'DELETE' });
