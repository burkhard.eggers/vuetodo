import Vue from 'vue';
import { MdSnackbar } from 'vue-material/dist/components'

Vue.use(MdSnackbar);

export default {
  template: `
      <MdSnackbar
              md-position="center"
              :md-active.sync="show"
              :md-duration="4000"
              md-persistent
      >
          <slot></slot>
      </MdSnackbar>
  `,
  props: {
    value: {
      type: Boolean,
      isRequired: true
    }
  },
  computed: {
    // convert v-model (prop value and event input into computed 'show)
    show: {
      get() {
        return !!this.value;
      },
      set(show) {
        this.$emit('input', !!show);
      }
    }
  }
}
