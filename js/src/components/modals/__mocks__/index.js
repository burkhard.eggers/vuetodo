export const prompt = () => {
  return new Promise(resolve => {
    resolve('prompt');
  });
};

export const alert = () => {
  return new Promise(resolve => {
    resolve();
  });
};

export const confirm = () => {
  return new Promise(resolve => {
    resolve(true);
  });
};
