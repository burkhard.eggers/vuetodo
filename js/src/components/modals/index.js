import jsLog from 'js-logger';
import getStore from '../../vuex/store';
import i18n from '../../i18n';

const log = jsLog.get('modals');

/* eslint-disable no-debugger */

// when the route changes and if the path remains and if modal=T  disappears (browser back when a modal is open),
// we must call the resolver of the current prompt
const before = (to, from, next) => {
  if (from.query.modal === 'T' && !to.query.modal && to.path === from.path) {
    log.info('remove modal');
    getStore().dispatch('modalsClose');
    resolver();
  }
  next();
};
let beforeSet = false;
let resolver = undefined;

// start route (add modal=T as query) if a prompt is opened
const modalsRoute = (router) => {
  if (!beforeSet) {
    beforeSet = true;
    router.beforeEach(before);
  }
  router.push({ query: { modal: 'T' } });
};

// when the prompt closes, we must remove the modal=T query
const removeParamResolver = (router, raw) => (arg) => {
  raw(arg);
  router.push({ query: { } });
};

// remove queries on init application
export const init = ({ $route, $router}) => {
  if($route.query.modal || $route.query.error) $router.push({ query: { } });
};

/* prompt. Get an input value */
export const prompt = (
  { $store, $router },
  { message, title, cancelText = i18n.t('modals.cancel'), okText = i18n.t('modals.ok') }
) => {
  log.info(`prompt ${message}`);
  modalsRoute($router);
  return new Promise(resolve => {
    resolver = resolve;
    $store.dispatch('prompt', { message, title, okText, cancelText, resolve: removeParamResolver($router, resolve) });
  });
};

/* alert. Show message */
export const alert = (
  { $store, $router },
  { message, title, okText = i18n.t('modals.ok') }
) => {
  log.info(`alert ${message}`);
  modalsRoute($router);
  return new Promise(resolve => {
    resolver = resolve;
    $store.dispatch('alert', { message, title, okText, resolve: removeParamResolver($router, resolve) });
  });
};

/* confirm. User chooses true or false */
export const confirm = (
  { $store, $router },
  { message, title, okText = i18n.t('modals.ok'), cancelText = i18n.t('modals.cancel') }
) => {
  log.info(`confirm ${message}`);
  modalsRoute($router);
  return new Promise(resolve => {
    resolver = resolve;
    $store.dispatch('confirm', { message, title, okText, cancelText, resolve: removeParamResolver($router, resolve) });
  });
};
