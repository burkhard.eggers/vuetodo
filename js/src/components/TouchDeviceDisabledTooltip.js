import Vue from 'vue';
import { MdTooltip } from 'vue-material/dist/components'

Vue.use(MdTooltip);

export default {
  template: '<MdTooltip v-if="!touch()"><slot></slot></MdTooltip>',
  methods: {
    touch(){
      return "ontouchstart" in document.documentElement;
    }
  }
}
