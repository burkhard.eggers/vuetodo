export default {
  sortTooltip: 'sort {order} by {column}',
  ascending: 'ascending',
  descending: 'descending',
  openDetailsTooltip: 'open details for {summary}',
  empty: 'no data',
  paginator: '{start} - {end} of {total}'
}
