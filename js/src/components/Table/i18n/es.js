export default {
  sortTooltip: 'ordenar {order} por {column}',
  ascending: 'ascendiendo',
  descending: 'descendiendo',
  empty: 'No hay nada',
  paginator: '{start} - {end} de {total}'
}
