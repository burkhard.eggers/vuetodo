import VueI18n from "vue-i18n";
import Vue from 'vue';
import de from './de';
import en from './en';
import es from './es';

Vue.use(VueI18n);

export default new VueI18n({
  locale: 'en',
  messages: {
    de, en, es
  }
});
