export default {
  sortTooltip: 'sortiere {order} nach {column}',
  ascending: 'aufsteigend',
  descending: 'absteigend',
  empty: 'keine Daten',
  paginator: '{start} - {end} von {total}'
}
