import jsLog from 'js-logger';

const log = jsLog.get('TableRegistry');

const tables = {};
export const getInstance = (tableId) => tables[tableId];

export const setInstance = (table) => {
  log.info('set instance', table.id);
  tables[table.id] = table;
};

export const releaseInstance = (tableId) => {
  log.info('release instance', tableId);
  delete tables[tableId];
}
