export default {
  state: {
    alert: {},
    confirm: {},
    prompt: {}
  },
  mutations: {
    alert(state, alert) {
      state.alert = alert;
    },
    confirm(state, confirm) {
      state.confirm = confirm;
    },
    prompt(state, prompt) {
      state.prompt = prompt;
    },
    modalsClose(state) {
      state.alert = {};
      state.confirm = {};
      state.prompt = {};
    }
  },
  actions: {
    alert({ commit }, alert) {
      commit('alert', alert);
    },
    confirm({ commit }, confirm) {
      commit('confirm', confirm);
    },
    prompt({ commit }, prompt) {
      commit('prompt', prompt);
    },
    modalsClose({ commit }) {
      commit('modalsClose');
    }
  }
};
