import Vuex from 'vuex';
import { getTodos, newTodo, deleteTodo, addSubtask } from '../Api';
import modals from './modalsStore';
import details from './detailsStore';
import request from './requestStore';
import login from './loginStore';

/* eslint-disable no-debugger */

let store;

export default () => {
  if (!store) store = new Vuex.Store({
    modules: {
      modals,
      details,
      request,
      login
    },
    state: {
      loading: true,
      todos: undefined
    },
    mutations: {
      setLoading(state, loading) {
        state.loading = loading;
      },
      setTodos(state, todos) {
        state.todos = todos;
      },
      commitGloballyDetails(state, todo){
        state.todos.forEach(rt => {
          if (rt.id === todo.id) {
            Object.keys(todo).forEach(key => {
              rt[key] = todo[key];
            });
            rt.subtasks = todo.subtasks.filter(st => state.details.subtasksReady.indexOf(st.id) === -1);
          }
        });
      },
      addSubTaskToTodo(state, { id, name }) {
        const todo = state.todos.find(({ id: tid }) => tid === id);
        todo.subtasks.push({ name });
      },
      deleteTodo(state, id) {
        const newTodos = state.todos.filter(todo => todo.id !== id);
        state.todos = newTodos;
      },
      addTodo(state, { id, name }) {
        state.lastAddedId = id;
        state.todos.push({ id, summary: name, subtasks: [] });
      },
    },
    actions: {
      async load({ commit, getters }, openTodoId) {
        const { data: todos } = await getTodos();
        commit('setTodos', todos);
        if(openTodoId) {
          const todo = getters.todos.find(t => t.id == openTodoId);
          if(todo) commit('openDetails', todo);
        }
        commit('setLoading', false);
      },
      async addSubTaskToTodo({ commit }, { id, name }) {
        await addSubtask(id, name);
        commit('addSubTaskToTodo', { id, name });
      },
      async deleteTodo({ commit }, id) {
        await deleteTodo(id);
        commit('deleteTodo', id);
      },
      async addTodo({ commit }, name) {
        const { data: id } = await newTodo(name);
        commit('addTodo', { id, name });
      },
      async deleteAndClose({ commit }, id) {
        await deleteTodo(id);
        commit('deleteTodo', id);
        commit('openDetails');
      }
    }
  });
  return store;
}
