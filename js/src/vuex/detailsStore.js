import { updateTodo } from "@/Api";
/* eslint-disable no-debugger */

export default {
  state: {
    openTodo: undefined,
    subtasksReady: [],
    errorInDetails: false
  },
  getters: {
    todos(state, getters, rootState) {
      return rootState.todos;
    }
  },
  mutations: {
    openDetails(state, todo) {
      if (!todo) {
        state.openTodo = undefined;
        state.subtasksReady = [];
        state.errorInDetails = false;
        return;
      }
      if (!todo) return;
      state.openTodo = { ...todo };
    },
    updateDetails(state, { fieldName, value }) {
      state.openTodo[fieldName] = value;
      state.errorInDetails = false;
    },
    commitDetails(state) {
      state.openTodo = undefined;
      state.errorInDetails = {};
    },
    ready(state, id) {
      state.subtasksReady.push(id);
    },
    unready(state, id) {
      state.subtasksReady = state.subtasksReady.filter(n => n !== id);
    },
    addSubTask(state, name) {
      state.openTodo.subtasks.push({ name });
    }
  },
  actions: {
    details({ commit, getters }, id) {
      const todo = getters.todos && getters.todos.find(t => t.id == id);
      commit('openDetails', todo);
    },
    async updateDetails({ commit }, pl) {
      await commit('updateDetails', pl);
    },
    async commitDetails({ commit, state, getters }) {
      const { openTodo } = state;
      const id = openTodo.id;
      commit('commitGloballyDetails', openTodo);
      commit('commitDetails');
      const todo = getters.todos.find(t => t.id === id);
      await updateTodo(todo);
    },
    ready({ commit }, id) {
      commit('ready', id);
    },
    unready({ commit }, id) {
      commit('unready', id);
    },
    async addSubTask({ commit }, name) {
      commit('addSubTask', name);
    }
  }
};
