export default {
  state: {
    user: '',
    password: ''
  },
  mutations: {
    user(state, user){
      state.user = user;
    },
    password(state, password){
      state.password = password;
    }
  },
  actions: {
    credentials({ commit }, { user, password }){
      commit('user', user);
      commit('password', password);
    }
  }
};
