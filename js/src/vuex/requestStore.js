export default {
  state: {
    active: false,
    error: undefined
  },
  mutations: {
    active(state, active){
      state.active = active;
    },
    showError(state){
      state.showError = true;
    },
    error(state, error){
      if(error) state.active = true;
      state.error = {...error, showSnack: true };
    },
    hideSnack(state){
      state.error.showSnack = false;
    },
    closeErrorDetails(state) {
      state.error = undefined;
      state.active = false;
    }
  },
  actions: {
    startRequest({ commit }){
      commit('active', true);
    },
    endRequest({ commit }){
      commit('active', false);
    },
    error({ commit }, error){
      commit('error', error);
    },
    hideSnack({ commit }){
      commit('active', false);
      commit('hideSnack')
    },
    closeErrorDetails({ commit }){
      commit('closeErrorDetails');
    }
  }
};
