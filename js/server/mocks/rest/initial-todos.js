module.exports = [
  {
    id: '1',
    summary: 'jiji',
    subtasks: []
  },
  {
    id: '2',
    summary: 'jojo',
    description: 'jojo ist viel zu tun!!',
    subtasks: [
      {
        name: 't1'
      }
    ]
  },
  {
    id: '3',
    summary: 'om mani',
    description: 'padme',
    subtasks: [
      {
        name: 'tibet'
      },
      {
        name: 'harrer'
      }
    ]
  },
  {
    id: '5',
    summary: 'sum 5',
    description: 'desc 5',
    subtasks: []
  }
]
