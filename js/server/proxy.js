var express = require('express');
var httpProxy = require('http-proxy');
var stringToStream = require('string-to-stream');
const bodyParser = require('body-parser');

module.exports = function ({ serviceName, restPort, proxyPort, uiPort }) {
  var app = express();
  var apiProxy = httpProxy.createProxyServer({});
  var frontend = `http://localhost:${uiPort}`;
  var backend = `http://localhost:${restPort}`;

  app.use(bodyParser.json());

  var services = [];
  serviceName.split(',').forEach(name => {
    services.push(name);
    services.push(`${name}/**`);
  });

  services.forEach(name => {
    const toBackend = `/${name.trim()}`;
    console.log(`${toBackend} -> ${backend}`);
    app.all(toBackend, function (req, res) {
      var buffer = undefined;
      console.log('request url', req.url, 'method', req.method, 'body', req.body);
      if (req.body && (req.method === 'POST' || req.method === 'PUT')) {
        buffer = stringToStream(JSON.stringify(req.body));
      }
      apiProxy.web(req, res, { target: backend, buffer });
    });
  });

  console.log(`all others -> ${frontend}`);
  app.all("/*", function (req, res) {
    apiProxy.web(req, res, { target: frontend });
  });

  var server = require('http').createServer(app);
  server.on('upgrade', function (req, socket, head) {
    apiProxy.ws(req, socket, head, { target: frontend });
  });
  server.listen(proxyPort);
};
