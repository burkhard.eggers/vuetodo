var http = require('http');
var mockserver = require('mockserver');
var proxy = require('./proxy');

var restPort = 3001;
var proxyPort = 3000;
var uiPort = 8080;

console.log(`start proxy on port ${proxyPort}...`);
proxy({serviceName: 'rest', restPort, proxyPort, uiPort });

console.log(`start mock server on port ${restPort}...`);
http.createServer(mockserver('./mocks')).listen(restPort);
