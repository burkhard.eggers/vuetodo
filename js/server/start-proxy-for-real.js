var proxy = require('./proxy');

var restPort = 9000;
var proxyPort = 3000;
var uiPort = 8080;
var serviceName = 'rest, auth';

console.log(`start proxy on port ${proxyPort}...`);
console.log(`expected rest service with service name ${serviceName} to be running on port ${restPort}...`);
proxy({serviceName, restPort, proxyPort, uiPort});
