import { shallowMount, createLocalVue } from '@vue/test-utils';
import Details from '../../../src/components/Details';
import { normalize, stubs, mocks } from '../../TestHelper';

jest.mock('../../../src/components/modals');
jest.mock('../../../src/components/Details/Subtasks', () => {
});
jest.mock('vue-material-design-icons/Delete', () => ({
  template: '<span>delete-icon</span>'
}));

describe('Details', () => {
  it('should render', () => {
    const localVue = createLocalVue();
    // Mount the component
    const wrapper = shallowMount(Details, {
      stubs,
      mocks: {
        ...mocks,
        $route: {
          params:{
            id: 1
          }
        },
        $store: {
          dispatch(){
          },
          state: {
            details: {
              errorInDetails: {
                summary: true
              }
            }
          }
        }
      },
      computed: {
        active() {
          return true;
        },
        title() {
          return 'title'
        },
        summary() {
          return 'summary'
        },
        description() {
          return 'description'
        }
      },
      localVue
    });

    const expected = normalize(`
      <div md-active="true" class="dialog">md-dialog:
        <div class="header">
          <div class="headerText">md-dialog-title: title</div>
          <div class="headerButton">
            <div class="md-icon-button md-dense">button:
              <span><span>delete-icon</span></span>
            </div>
            <div>tooltip: i18n: details.deleteTodoTooltip</div>
          </div>
        </div>
        <div>
            md-dialog-content:
          <div class="md-invalid">md-field:
            <label>i18n: summary</label>
            <div placeholder="i18n: details.summaryPlaceholder" required="required">md-input: summary</div>
            <span class="md-error">i18n: details.requiredField</span>
          </div>
          <div>md-field:
            <label>i18n: description</label>
            <div placeholder="i18n: details.descriptionPlaceholder">md-textarea: description</div>
          </div>
          <div>Subtasks</div>
        </div>
        <div>
            md-dialog-actions:
          <div>button:
            <span>i18n: details.save</span>
          </div>
          <div>button:
            <span>i18n: details.close</span>
          </div>
        </div>
      </div>
    `);
    expect(normalize(wrapper.html())).toBe(expected);
  })
});
