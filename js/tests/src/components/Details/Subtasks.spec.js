import { shallowMount, createLocalVue } from '@vue/test-utils';
import Subtasks from '../../../../src/components/Details/Subtasks';
import { normalize, stubs, mocks } from '../../../TestHelper';

jest.mock('../../../../src/components/modals');
jest.mock('../../../../src/components/Table', () => {});

describe('Subtasks', () => {
  it('should render', () => {
    const localVue = createLocalVue();
    // Mount the component
    const wrapper = shallowMount(Subtasks, {
      stubs,
      mocks: {
        ...mocks,
        $store: {
          state: {
            details: {
              openTodo: {
                subtasks: [
                  { name: 'st1', id: 1 },
                  { name: 'st2', id: 2 }
                ]
              }
            }
          }
        }
      },
      localVue
    });

    const expected = normalize(`
    <div>
      <div class="header">
        <div class="headerLabel md-subheading">i18n: subtasks.header</div>
        <div class="headerButton">
          <div class="md-icon-button md-dense md-raised md-primary">
            button:<span>+</span>
          </div>
          <div>tooltip: i18n: subtasks.addTooltip</div>
        </div>
      </div>
      <div>Table:2 rows</div>
    </div>
    `);
    expect(normalize(wrapper.html())).toBe(expected);
  })
});
