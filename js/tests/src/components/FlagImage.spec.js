import { shallowMount } from '@vue/test-utils';

import FlagImage from '../../../src/components/FlagImage';

describe('FlagImage', () => {
  it('should render', () => {
    const wrapper = shallowMount(FlagImage, {
      propsData: {lang: 'de', small: true}
    });
    expect(wrapper.html()).toBe('<span><img src="./images/de.png" width="15px"></span>');
  })
});
