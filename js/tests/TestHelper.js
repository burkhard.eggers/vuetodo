import Table from './mocks/Table';
import Subtasks from './mocks/Subtasks';
import components from '../__mocks__/vue-material/dist/components';

export const normalize = (text) => {
  let t = text;
  const re = new RegExp('>\\s+<', 'g');
  t = t.replace(re, '><').trim();
  const re2 = new RegExp('\\s+<', 'g');
  t = t.replace(re2, '<').trim();
  const re3 = new RegExp('>\\s+', 'g');
  t = t.replace(re3, '>').trim();
  return t;
};

export const stubs = {
  Table,
  'todo-subtasks': Subtasks,
  DeleteIcon: {
    template: '<span>delete-icon</span>'
  },
  ...components
};

export const mocks = {
  $t: key => `i18n: ${key}`
};
