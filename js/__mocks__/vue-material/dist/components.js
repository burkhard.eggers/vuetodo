module.exports = {
  MdButton: { template: '<div>button: <span><slot></slot></span></div>' },
  MdCheckbox: { template: '<div>checkbox: {{value}}</div>', props: ['value'] },
  MdTooltip: { template: '<div>tooltip: <slot></slot></div>' },
  MdDialog: { template: '<div>md-dialog: <slot></slot></div>'},
  MdDialogActions: { template: '<div>md-dialog-actions: <slot></slot></div>'},
  MdDialogContent: { template: '<div>md-dialog-content: <slot></slot></div>'},
  MdDialogTitle: { template: '<div>md-dialog-title: <slot></slot></div>'},
  MdField: { template: '<div>md-field: <slot></slot></div>'},
  MdTextarea: { template: '<div>md-textarea: {{value}}</div>', props: ['value'] },
  MdInput: { template: '<div>md-input: {{value}}</div>', props: ['value'] }
};
