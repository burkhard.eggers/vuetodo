package com.todo.vuetodo.login;

import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class Encoder {
    public String encode(String raw){
        return Base64.getEncoder().encodeToString(raw.getBytes());
    }
}
