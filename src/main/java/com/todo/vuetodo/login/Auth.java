package com.todo.vuetodo.login;

import javax.naming.AuthenticationException;

public interface Auth {
    boolean isLoggedIn(String token);

    String authenticate(String user, String passwordHash) throws AuthenticationException;

    void logout(String token);
}
