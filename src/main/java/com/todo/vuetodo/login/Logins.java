package com.todo.vuetodo.login;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.naming.AuthenticationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component("logins")
public class Logins implements Auth{

    @Autowired
    private Encoder encoder;

    @Override
    public boolean isLoggedIn(String token) {
        return currentlyLoggedIn.contains(token);
    }

    @Override
    public String authenticate(String user, String passwordHash) throws AuthenticationException {
        boolean ok = passwordHash.equals(credentials().get(user));
        if(!ok) throw new AuthenticationException();
        String token = UUID.randomUUID().toString();;
        currentlyLoggedIn.add(token);
        return token;
    }

    @Override
    public void logout(String token){
        currentlyLoggedIn.remove(token);
    }


    private Map<String, String> credentials() {
        return ImmutableMap.of(
                "burkhard", encoder.encode("burkhard"),
                "admin", encoder.encode("admin"),
                "bu", encoder.encode("buo"));

    }
    private List<String> currentlyLoggedIn = new ArrayList<>();


}
