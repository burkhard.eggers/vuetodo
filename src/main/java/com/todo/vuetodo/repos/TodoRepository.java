package com.todo.vuetodo.repos;

import com.todo.vuetodo.entities.Todo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
//hh
import java.util.List;
import java.util.stream.Stream;

public interface TodoRepository extends CrudRepository<Todo, Long> {
    @Query("SELECT t FROM Todo t WHERE EXISTS " +
            "(SELECT st from Subtask st WHERE st.name = :stname AND st.todo.id = t.id)")
    Stream<Todo> findTodoWithSubtaskWithName(@Param("stname") String stName);

    List<Todo> findByDescription(String description);
}
