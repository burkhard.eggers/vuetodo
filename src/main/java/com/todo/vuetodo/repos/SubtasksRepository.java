package com.todo.vuetodo.repos;

import com.todo.vuetodo.entities.Subtask;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SubtasksRepository extends CrudRepository<Subtask, Long> {
    Optional<Subtask> findByName(String name);
}
