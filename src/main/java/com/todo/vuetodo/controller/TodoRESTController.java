package com.todo.vuetodo.controller;

import com.todo.vuetodo.repos.SubtasksRepository;
import com.todo.vuetodo.repos.TodoRepository;
import com.todo.vuetodo.entities.Subtask;
import com.todo.vuetodo.entities.Todo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Validated
@RestController
@RequestMapping("/rest")
//
public class TodoRESTController {

    @Autowired TodoRepository todorep;
    @Autowired
    SubtasksRepository subtasksRepository;

    // default
    @GetMapping
    public Object allTodos() throws InterruptedException {
        log.info("default called");
        Thread.sleep(2000);
        //throw new IllegalStateException("some bad stuff happened!");
        return StreamSupport.stream(todorep.findAll().spliterator(), false);
    }

    @DeleteMapping("/{id}")
    public String deleteTodo(@PathVariable("id") Long id){
        log.info("delete called for id {}", id);
        todorep.deleteById(id);
        return "ok";
    }

    private Long save(Todo ft){
        ft.getSubtasks().forEach(subTask -> subTask.setTodo(ft));
        return todorep.save(ft).getId();
    }

    @PutMapping
    public void updateTodo(@RequestBody Todo todo) throws InterruptedException {
        Thread.sleep(3000);
        //if(true) throw new IllegalStateException("some bad stuff happened!");
        Optional<Todo> found = todorep.findById(todo.getId());
        if(found.isPresent()) {
            List<Long> stIdsThere = todo.getSubtasks().stream().map(st -> st.getId()).collect(Collectors.toList());
            List<Long> subTasksToRemove =
                    found.get().getSubtasks().stream().filter(st -> !stIdsThere.contains(st.getId()))
                            .map(st -> st.getId()).collect(Collectors.toList());
            Todo ft = found.get();
            ft.setSummary(todo.getSummary());
            ft.setDescription(todo.getDescription());
            ft.setSubtasks(todo.getSubtasks());
            save(ft);
            subTasksToRemove.forEach(subtasksRepository::deleteById);
        }
    }

    @PostMapping
    public Long createTodo( @RequestBody Todo todo){
        return save(todo);
    }

    @PutMapping("/{id}/subtask")
    public void addSubtask(@PathVariable("id") Long id, @RequestBody Subtask subtask){
        log.info("add subtask", id, subtask.getName());
        Optional<Todo> found = todorep.findById(id);
        if(found.isPresent()) {
            Todo ft = found.get();
            ft.getSubtasks().add(subtask);
            save(ft);
        }
    }

}
