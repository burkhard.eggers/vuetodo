package com.todo.vuetodo.controller;

import com.google.common.base.Charsets;
import com.google.common.io.ByteSource;
import com.google.common.io.ByteStreams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;


@Slf4j
@Validated
@RequestMapping("/**")
@Controller
public class StaticRessourcesController {

    @GetMapping(value={"/css/**", "js/**"})
    public ResponseEntity<String> asciiRessource(HttpServletRequest req) throws IOException {
        return getClasspathRessourceAsString("public" + req.getRequestURI());
    }

    @GetMapping(value={"/**/*.ico", "/**/*.jpeg", "/**/*.png", "/**/*.jpg","/**/*.gif" })
    public ResponseEntity<byte[]> binaryRessource(HttpServletRequest req) throws IOException {
        String path = "public" + req.getRequestURI();
        log.info("binary ressource {}", path);
        InputStream inputStream = new ClassPathResource(path).getInputStream();
        byte[] bytes = ByteStreams.toByteArray(inputStream);
        return new ResponseEntity<byte[]>(bytes, HttpStatus.OK);
    }

    /* all that is not served by the rest controller and that is not within above classpath
    patterns is considered index.html */
    @GetMapping
    public Object index(HttpServletRequest req) throws IOException {
        log.info("request {} serve as index.html", req.getRequestURI());
        return getClasspathRessourceAsString("public/index.html");
    }

    private ResponseEntity<String> getClasspathRessourceAsString(String path) throws IOException{
        log.info("utf8 ressource {}", path);
        InputStream inputStream = new ClassPathResource(path).getInputStream();
        ByteSource byteSource = new ByteSource() {
            @Override
            public InputStream openStream() throws IOException {
                return inputStream;
            }
        };
        String text = byteSource.asCharSource(Charsets.UTF_8).read();

        return new ResponseEntity<String>(text, HttpStatus.OK);
    }

}
