package com.todo.vuetodo.controller.login;

import java.io.Serializable;

public class LoginCredentials implements Serializable {
    public String user;
    public String passwordHash;
}
