package com.todo.vuetodo.controller.login;

import com.todo.vuetodo.config.filter.AuthRedirectFilter;
import com.todo.vuetodo.login.Auth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Slf4j
@Validated
@RestController
@RequestMapping("/auth")
public class LoginController {

    @Autowired
    private Auth auth;

    // default
    @PostMapping
    public String login(HttpServletResponse response,  @RequestBody LoginCredentials credentials){
        log.info("login called", credentials);
        try {
            String token = auth.authenticate(credentials.user, credentials.passwordHash);
            Cookie cookie = new Cookie(AuthRedirectFilter.COOKIE_NAME, token);
            response.addCookie(cookie);
        } catch (AuthenticationException e) {
            log.warn("authentication failed, user {}, pass hash {}",credentials.user, credentials.passwordHash );
            return "not";
        }
        return "ok";
    }

    @DeleteMapping
    public void logout(HttpServletRequest request){
        String token = AuthRedirectFilter.tokenFromRequest(request);
        auth.logout(token);

    }
}
