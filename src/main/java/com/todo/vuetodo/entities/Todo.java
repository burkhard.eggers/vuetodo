package com.todo.vuetodo.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Todo")
@Setter
@Getter
@Accessors(chain = true)
@ToString
public class Todo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String summary;
    private String description;

    @OneToMany(mappedBy = "todo", cascade = CascadeType.ALL)
    private List<Subtask> subtasks = new ArrayList<>();
    public void addSubTask(Subtask subtask){
        subtasks.add(subtask);
    }

}
