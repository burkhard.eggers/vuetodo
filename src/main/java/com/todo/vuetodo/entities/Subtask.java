package com.todo.vuetodo.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Subtask")
@Accessors(chain = true)
@Setter
@Getter
public class Subtask implements Serializable {

    @ManyToOne
    @JsonIgnore
    private Todo todo;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(name = "name")
    private String name;

}


