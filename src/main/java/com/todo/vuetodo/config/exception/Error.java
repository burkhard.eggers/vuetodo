package com.todo.vuetodo.config.exception;

import java.io.Serializable;

public class Error implements Serializable {

    public String errorMsg;
    public String exceptionType;

    public Error(String errorMsg, String exceptionType) {
        this.errorMsg = errorMsg;
        this.exceptionType = exceptionType;
    }
}
