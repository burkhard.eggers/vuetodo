package com.todo.vuetodo.config.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class TodoExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value
            = { RuntimeException.class })
    protected ResponseEntity<Object> handle(
            RuntimeException ex, WebRequest request) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = "";
        try {
            json = objectMapper.writeValueAsString(new Error(ex.getMessage(), ex.getClass().getCanonicalName()));
        } catch (JsonProcessingException e) {
            log.error("error producing error response", e);
            json = "{}";
        }
        return handleExceptionInternal(ex, json,
                httpHeaders, HttpStatus.CONFLICT, request);
    }
}
