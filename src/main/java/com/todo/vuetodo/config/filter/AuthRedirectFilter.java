package com.todo.vuetodo.config.filter;

import com.todo.vuetodo.login.Logins;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class AuthRedirectFilter implements Filter {

    public static final String COOKIE_NAME = "TODO_AUTH_TOKEN";

    public static String tokenFromRequest(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        String token = null;
        if(cookies != null) for (Cookie cookie: cookies) {
            if(cookie.getName().equals(COOKIE_NAME)){
                token = cookie.getValue();
            }
        }
        return token;
    }

    @Autowired
    private Logins logins;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String uri = httpServletRequest.getRequestURI();
        // css, js, or root is allowed
        boolean allowed = uri.equals("/") || uri.startsWith("/css") || uri.startsWith("/js");

        // the authenticate request ois also allowed
        boolean authRequest = uri.startsWith("/auth");

        if(allowed || authRequest) {
            chain.doFilter(request,response);
            return;
        }
        // images are also allowed
        Path path = new File(uri).toPath();
        String mime = Files.probeContentType(path);
        boolean isImage = mime != null && mime.startsWith("image/");
        if(isImage) {
            chain.doFilter(request,response);
            return;
        }

        // get the token from cookie and check if it is among the currently logged in ser tokens
        String token = tokenFromRequest(httpServletRequest);
        if(logins.isLoggedIn(token) ) {
            chain.doFilter(request, response);
            return;
        }

        // all others are not allowed
        ((HttpServletResponse)response).sendError(HttpStatus.UNAUTHORIZED.value());
    }

    @Override
    public void destroy() {

    }
}
