package com.todo;

import com.todo.vuetodo.controller.TodoRESTController;
import com.todo.vuetodo.entities.Subtask;
import com.todo.vuetodo.entities.Todo;
import com.todo.vuetodo.repos.SubtasksRepository;
import com.todo.vuetodo.repos.TodoRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class TodoControllerTest {
    @Configuration
    @Ignore
    public static class ContextConfiguration {
        @Bean
        TodoRepository todoRepo() {
            return Mockito.mock(TodoRepository.class);
        }

        @Bean
        SubtasksRepository subtasksRepository() {
            return Mockito.mock(SubtasksRepository.class);
        }

        @Bean
        TodoRESTController todoController() {
            return new TodoRESTController();
        }
    }

    @Autowired
    TodoRepository todoRepository;
    @Autowired
    SubtasksRepository subtasksRepository;
    @Autowired
    TodoRESTController todoController;

    @Test
    public void addSubtasktest() {
        Long TODO_ID_THERE = 12L;
        Long TODO_ID_NOT_THERE = 13L;
        Todo todoThere = new Todo().setId(TODO_ID_THERE).setSubtasks(new ArrayList<>());
        Todo todoNotThere = new Todo().setId(TODO_ID_NOT_THERE).setSubtasks(new ArrayList<>());
        String SUBTASK_EXIST_NAME = "stname%%%";
        Mockito.when(todoRepository.findById(TODO_ID_THERE)).thenReturn(Optional.of(todoThere));
        Mockito.when(todoRepository.findById(TODO_ID_NOT_THERE)).thenReturn(Optional.empty());
        Mockito.when((todoRepository.save(Mockito.any()))).thenAnswer(invokation -> invokation.getArgument(0));

        Subtask newTask = new Subtask().setName(SUBTASK_EXIST_NAME);
        todoController.addSubtask(TODO_ID_THERE, newTask);
        Assert.assertEquals(1, todoThere.getSubtasks().size());

        todoController.addSubtask(TODO_ID_NOT_THERE, newTask);
        Assert.assertEquals(0, todoNotThere.getSubtasks().size());

    }

    private Todo saveInvokedTodo = null;

    @Test
    public void updateTodoTest() {
        Long TODO_ID = 12L;
        String NEW_DESCRIPTION = "drtrd";
        String NEW_SUMMARY = "koko";
        Subtask removed = new Subtask().setName("removed").setId(1L);
        Subtask remain = new Subtask().setName("remain").setId(2L);
        Subtask newOne = new Subtask().setName("newOne").setId(3L);

        /*
        Todo todoThere = new Todo().setSummary("summold").setDescription("descold").setId(TODO_ID).setSubtasks(
                List.of(removed, remain)
        );
        Todo todoChanged = new Todo().setSummary(NEW_SUMMARY).setDescription(NEW_DESCRIPTION).setId(TODO_ID).setSubtasks(
                List.of(remain, newOne)
        );
        Mockito.when(todoRepository.findById(TODO_ID)).thenReturn(Optional.of(todoThere));


        Mockito.when(todoRepository.save(Mockito.any())).thenAnswer(new Answer<Todo>() {
            @Override
            public Todo answer(InvocationOnMock invocation) throws Throwable {
                saveInvokedTodo = invocation.getArgument(0);
                return saveInvokedTodo;
            }
        });

        try {
            todoController.updateTodo(todoChanged);
        } catch (InterruptedException e) {
            Assert.fail();
        }


         */
        //must have called deletPerId once and with the id of the removed subtask
        Mockito.verify(subtasksRepository, Mockito.times(1)).deleteById(1L);
        Mockito.verify(subtasksRepository, Mockito.times(1)).deleteById(Mockito.any());

        // save must have been invoked with the changed todo
        Assert.assertEquals(NEW_DESCRIPTION, saveInvokedTodo.getDescription());
        Assert.assertEquals(NEW_SUMMARY, saveInvokedTodo.getSummary());
        Assert.assertEquals(2, saveInvokedTodo.getSubtasks().size());

    }
}